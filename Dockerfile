FROM golang:1.14.2-alpine3.11 as build
COPY . /run
RUN apk add --update --no-cache git && cd /run && go build

FROM alpine:3.11 as run
COPY --from=build /run/image_test /usr/local/bin/image_test
EXPOSE 8901
ENTRYPOINT image_test