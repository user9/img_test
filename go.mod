module image_test

go 1.14

require (
	github.com/disintegration/imaging v1.6.2
	github.com/google/uuid v1.1.1
	github.com/gorilla/mux v1.7.4
	github.com/stretchr/testify v1.5.1
)
