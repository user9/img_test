package middleware

import (
	"net/http"
	"strings"

	"image_test/model/dao"
)

func Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method == http.MethodGet {
			if r.FormValue("img_url") == "" {
				http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
				return
			}
		}
		if r.Method == http.MethodPost {
			contentType := r.Header.Get("Content-Type")

			if contentType != dao.JsonType && !strings.HasPrefix(contentType, dao.FormType) {
				http.Error(w, http.StatusText(http.StatusUnsupportedMediaType), http.StatusUnsupportedMediaType)
				return
			}
		}

		next.ServeHTTP(w, r)
	})
}
