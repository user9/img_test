package dao

type ImgData struct {
	Image string `json:"image"`
	Type  string `json:"type"`
}

type ApiError struct {
	Status   int      `json:"status"`
	Messages []string `json:"messages"`
}
