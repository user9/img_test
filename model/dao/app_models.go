package dao

import (
	"fmt"
	"os"
)

const (
	JsonType = "application/json"
	FormType = "multipart/form-data"
)

var AllowedContentTypes = map[string]bool{
	JsonType: true,
	FormType: true,
}

type Config struct {
	Address          string
	ImagePath        string
	ImagePreviewPath string
	ImageWidth       int
	ImageHeight      int
}

func (c Config) IsValid() error {
	if c.Address == "" {
		return fmt.Errorf("Address is empty")
	}
	if c.ImagePreviewPath == "" {
		return fmt.Errorf("ImagePreviewPath  is empty")
	}
	if c.ImagePath == "" {
		return fmt.Errorf("ImagePath  is empty")
	}
	if _, err := os.Stat(c.ImagePreviewPath); os.IsNotExist(err) {
		os.MkdirAll(c.ImagePreviewPath, 0777)
	}
	if _, err := os.Stat(c.ImagePath); os.IsNotExist(err) {
		os.MkdirAll(c.ImagePath, 0777)
	}
	if c.ImageHeight == 0 {
		return fmt.Errorf("ImageHeight is 0")
	}
	if c.ImageWidth == 0 {
		return fmt.Errorf("ImageWidth is 0")
	}

	return nil
}

type ImageData struct {
	Name string
	Ext  string
}
