package service

import (
	"fmt"
	"log"
	"os"
	"strings"

	"image_test/model/dao"

	"github.com/disintegration/imaging"
)

func resize(filePath string) {
	sourceImage, err := imaging.Open(filePath)

	if err != nil {
		log.Println("Open saved image for resize err:", err)
		return
	}

	imageData, err := getImageData(filePath)

	if err != nil {
		log.Println(err)
		return
	}

	name := fmt.Sprintf("%s.%s", imageData.Name, imageData.Ext)
	format := formats[imageData.Ext]
	resizeNRGBA := imaging.Resize(sourceImage, config.ImageWidth, config.ImageHeight, imaging.Linear)
	file, err := os.OpenFile(fmt.Sprintf("%s/%s", config.ImagePreviewPath, name), os.O_WRONLY|os.O_CREATE, 0777)

	if err != nil {
		log.Println("Create file for resized image err:", err)
		return
	}
	if err := imaging.Encode(file, resizeNRGBA, format); err != nil {
		log.Println("encode image err:", err)
	}
	if err := file.Close(); err != nil {
		log.Println("close resized file err:", err)
	}
}

func getImageData(path string) (dao.ImageData, error) {
	imgData := dao.ImageData{}
	pathChunks := strings.FieldsFunc(path, func(r rune) bool {
		return r == '/' || r == '\\'
	})

	if len(pathChunks) < 2 {
		return imgData, fmt.Errorf("invalid file path: %s", path)
	}

	fileName := pathChunks[len(pathChunks)-1]
	nameArr := strings.Split(fileName, ".")

	if len(nameArr) != 2 {
		return imgData, fmt.Errorf("invalid file name: %s", fileName)
	}

	imgData.Name = nameArr[0]
	imgData.Ext = nameArr[1]

	return imgData, nil
}
