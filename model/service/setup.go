package service

import (
	"log"

	"image_test/model/dao"
)

var config dao.Config
var resizeChan chan string
var doneChan chan bool

func Setup(cfg dao.Config) {
	config = cfg
	resizeChan = make(chan string, 5)
	doneChan = make(chan bool, 1)

	go listeners()
}

func Shutdown() {
	doneChan <- true
}

func listeners() {
	for {
		select {
		case filePath := <-resizeChan:
			go resize(filePath)
		case <-doneChan:
			log.Println("services shutdown")
			close(resizeChan)
			close(doneChan)
			return
		}
	}
}
