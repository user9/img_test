package service

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"
	"os"
	"strings"

	"image_test/model/dao"

	"github.com/disintegration/imaging"
	"github.com/google/uuid"
)

var formats = map[string]imaging.Format{
	"jpeg": imaging.JPEG,
	"jpg":  imaging.JPEG,
	"gif":  imaging.GIF,
	"bmp":  imaging.BMP,
	"tiff": imaging.TIFF,
	"png":  imaging.PNG,
}

func SaveFromJson(data []dao.ImgData) []error {
	errors := make([]error, 0)

	for _, item := range data {
		imageBytes, err := base64.StdEncoding.DecodeString(item.Image)

		if err != nil {
			log.Println("decode base64 from string err:", err)
			errors = append(errors, fmt.Errorf("base64 for image\n %s \ninvalid", item.Image))
			continue
		}

		reader := bytes.NewReader(imageBytes)
		decoded, err := imaging.Decode(reader)

		if err != nil {
			log.Println("decode base64 to image err:", err)
			errors = append(errors, fmt.Errorf("base64 for image\n %s \ninvalid", item.Image))
			continue
		}

		uid := uuid.New().String()
		imageType := strings.ToLower(item.Type)
		format, ok := formats[imageType]

		if !ok {
			log.Println("unknown format", imageType)
			errors = append(errors, fmt.Errorf("unknown format for image\n %s", item.Image))
			continue
		}

		filePath := fmt.Sprintf("%s/%s.%s", config.ImagePath, uid, format.String())
		file, err := os.OpenFile(filePath, os.O_WRONLY|os.O_CREATE, 0777)

		if err != nil {
			log.Println("create file from base64 err:", err)
			errors = append(errors, fmt.Errorf("create file error"))
			continue
		}
		if err := imaging.Encode(file, decoded, format); err != nil {
			log.Println("encode image err:", err)
			errors = append(errors, fmt.Errorf("create file error"))

			if err := file.Close(); err != nil {
				log.Println("close file err:", err)
			}

			continue
		}
		if err := file.Close(); err != nil {
			log.Println("close file err:", err)
		}

		resizeChan <- filePath
	}

	return errors
}

func SaveFromForm(form *multipart.Form) []error {
	errors := make([]error, 0)

	for _, item := range form.File["images"] {
		file, err := item.Open()

		if err != nil {
			log.Println("open file from form err:", err)
			errors = append(errors, fmt.Errorf("open file %s error", item.Filename))
			continue
		}

		filePath := fmt.Sprintf("%s/%s", config.ImagePath, item.Filename)
		dst, err := os.Create(filePath)

		if err != nil {
			log.Println("create destination file err:", err)
			errors = append(errors, fmt.Errorf("create file %s error", item.Filename))

			if err := file.Close(); err != nil {
				log.Println("close file err:", err)
			}

			continue
		}
		if _, err := io.Copy(dst, file); err != nil {
			log.Println("copy file to destination err:", err)
			errors = append(errors, fmt.Errorf("create file %s error", item.Filename))

			if err := file.Close(); err != nil {
				log.Println("close file err:", err)
			}

			continue
		}
		if err := dst.Close(); err != nil {
			log.Println("close dst err:", err)
		}
		if err := file.Close(); err != nil {
			log.Println("close file err:", err)
		}

		resizeChan <- filePath
	}

	return errors
}

func SaveFromUrl(imageUrl string) error {
	response, err := http.Get(imageUrl)

	if err != nil {
		log.Println("image load err:", err)
		return fmt.Errorf("image load error")
	}

	defer response.Body.Close()
	body, err := ioutil.ReadAll(response.Body)

	if err != nil {
		log.Println("read img body err:", err)
		return fmt.Errorf("read image error")
	}

	uid := uuid.New().String()
	urlArr := strings.Split(imageUrl, ".")

	if len(urlArr) < 2 {
		return fmt.Errorf("invalid file name: %s", imageUrl)
	}

	ext := urlArr[len(urlArr)-1]

	if _, ok := formats[strings.ToLower(ext)]; !ok {
		return fmt.Errorf("not allow format: %s", ext)
	}

	name := fmt.Sprintf("%s/%s.%s", config.ImagePath, uid, ext)

	if err := ioutil.WriteFile(name, body, 0777); err != nil {
		log.Println("read img body err:", err)
		return fmt.Errorf("create image error")
	}

	resizeChan <- name
	return nil
}
