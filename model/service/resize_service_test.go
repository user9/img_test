package service

import (
	"fmt"
	"image"
	"image/color"
	"image/png"
	"io/ioutil"
	"os"
	"testing"

	"image_test/model/dao"

	"github.com/stretchr/testify/assert"
)

func createImg(name string, config dao.Config) {
	width := 200
	height := 100
	upLeft := image.Point{}
	lowRight := image.Point{X: width, Y: height}
	cyan := color.RGBA{R: 100, G: 200, B: 200, A: 0xff}
	img := image.NewRGBA(image.Rectangle{Min: upLeft, Max: lowRight})

	for x := 0; x < width; x++ {
		for y := 0; y < height; y++ {
			switch {
			case x < width/2 && y < height/2: // upper left quadrant
				img.Set(x, y, cyan)
			case x >= width/2 && y >= height/2: // lower right quadrant
				img.Set(x, y, color.White)
			default:
				// Use zero value.
			}
		}
	}

	newImg, _ := os.Create(config.ImagePath + "/" + name)
	png.Encode(newImg, img)
	newImg.Close()
}

func TestResizeValid(t *testing.T) {
	config = dao.Config{
		ImagePath:        "./images",
		ImagePreviewPath: "./preview",
		ImageWidth:       100,
		ImageHeight:      100,
	}
	os.Mkdir(config.ImagePath, 0777)
	os.Mkdir(config.ImagePreviewPath, 0777)
	defer os.RemoveAll(config.ImagePath)
	defer os.RemoveAll(config.ImagePreviewPath)

	createImg("image.png", config)
	resize("./images/image.png")

	assert.FileExists(t, "./preview/image.png")
}

func TestGetImageDataValid(t *testing.T) {
	filePath := "/preview/image.png"
	data, err := getImageData(filePath)

	assert.Nil(t, err)
	assert.Equal(t, "image", data.Name)
	assert.Equal(t, "png", data.Ext)

	filePath = "\\preview\\image.png"
	data, err = getImageData(filePath)

	assert.Nil(t, err)
	assert.Equal(t, "image", data.Name)
	assert.Equal(t, "png", data.Ext)
}

func TestResizeInvalid(t *testing.T) {
	config = dao.Config{
		ImagePath:        "./images",
		ImagePreviewPath: "./preview",
		ImageWidth:       100,
		ImageHeight:      100,
	}

	os.Mkdir(config.ImagePath, 0777)
	os.Mkdir(config.ImagePreviewPath, 0777)
	defer os.RemoveAll(config.ImagePath)
	defer os.RemoveAll(config.ImagePreviewPath)

	// invalid path
	resize("invalid path")
	files, _ := ioutil.ReadDir(config.ImagePreviewPath)
	assert.Empty(t, files)

	// invalid preview path
	config.ImagePreviewPath = "/nothing"
	createImg("image.png", config)
	resize(fmt.Sprintf("%s/%s", config.ImagePath, "image.png"))
	files, _ = ioutil.ReadDir(config.ImagePreviewPath)
	assert.Empty(t, files)
	config.ImagePreviewPath = "./preview"

	// invalid format
	//createImg("image.wbp", config)
	//resize(fmt.Sprintf("%s/%s", config.ImagePath, "image.wbp"))
	//files, _ = ioutil.ReadDir(config.ImagePreviewPath)
	//assert.Empty(t, files)

	// invalid file name
	createImg("image.jpg.png", config)
	resize(fmt.Sprintf("%s/%s", config.ImagePath, "image.jpg.png"))
	files, _ = ioutil.ReadDir(config.ImagePreviewPath)
	assert.Empty(t, files)
}

func TestGetImageDataInvalid(t *testing.T) {
	// invalid separator
	path := "path.to.image.png"
	_, err := getImageData(path)
	assert.NotNil(t, err)

	// invalid filename
	path = "path/to/image"
	_, err = getImageData(path)
	assert.NotNil(t, err)

	path = "path/to/image.png.png"
	_, err = getImageData(path)
	assert.NotNil(t, err)
}
