package handler

import (
	"bytes"
	"image"
	"image/color"
	"image/png"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"
	"time"

	"image_test/model/dao"
	"image_test/model/service"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
)

func TestIndexGetValid(t *testing.T) {
	wd, _ := os.Getwd()
	config := dao.Config{
		ImagePath:        wd + "/images",
		ImagePreviewPath: wd + "/previews",
		ImageWidth:       0,
		ImageHeight:      0,
	}
	service.Setup(config)
	os.Mkdir(config.ImagePreviewPath, 0777)
	os.Mkdir(config.ImagePath, 0777)
	defer os.RemoveAll(config.ImagePath)
	defer os.RemoveAll(config.ImagePreviewPath)

	request := httptest.NewRequest(http.MethodGet, "/", nil)
	request = mux.SetURLVars(request, map[string]string{
		"img_url": "https://cdn.chrisshort.net/testing-certificate-chains-in-go/GOPHER_MIC_DROP.png",
	})
	recorder := httptest.NewRecorder()
	IndexGet(recorder, request)
	time.Sleep(2 * time.Second)

	response := recorder.Result()
	assert.Equal(t, http.StatusOK, response.StatusCode)

	images, _ := ioutil.ReadDir(config.ImagePath)
	previews, _ := ioutil.ReadDir(config.ImagePreviewPath)

	assert.NotEmpty(t, images)
	assert.NotEmpty(t, previews)

	service.Shutdown()
}

func TestIndexPostValid(t *testing.T) {
	wd, _ := os.Getwd()
	config := dao.Config{
		ImagePath:        wd + "/images",
		ImagePreviewPath: wd + "/previews",
		ImageWidth:       0,
		ImageHeight:      0,
	}
	service.Setup(config)
	os.Mkdir(config.ImagePreviewPath, 0777)
	os.Mkdir(config.ImagePath, 0777)
	defer os.RemoveAll(config.ImagePath)
	defer os.RemoveAll(config.ImagePreviewPath)

	// json
	data := `
	[
		{
			"image": "iVBORw0KGgoAAAANSUhEUgAAADQAAABGCAYAAAH0lYe3AAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAA7DAAAOwwHHb6hkAAAAB3RJTUUH4wUQAyI0QL0XrwAACs9JREFUaIHtmn9sVFUWxz/nvjfT6XT42fJDa4XKT7u1GhAhaNBkzepqJBrdjT8wcUUaRUmMsgn46x9UjBiJu4lAty5GTExMVrOsicSN0fgXWRQ1VMVfsFRA3Frb2k7nx3vv3v3jzZtOOzPttB2lGr7J/DHv3nPPPfece+6551wBaG5+35CDlpZlNhhPDW3wcZcDYOc3wIoVywVWmIKNTU0XAaCCD+edV8WNN9YiAuvXzxvcKAJz5kQBsCzxvxWeEIMpR90o69Yd0CIi2d5K6Z07l1gq96M/KVFwbypvONd1WbFiSTivQURoarpwYLk6O1O8/PJyIhGL5ub3/Znt2rUUESESsbJURYUtKkvRhjwBC8GyLLNjx98F/ioA9kgEAMYYgRTQrJVKfjCshkqed21thHTaGx1ROGzR1+fheYbKSkUoJCSTHlddNasYkeG+++YRjVpYFvT3e7iuYevWC3AcjTG+JOWRqSQiywo7oyWS9evbUq6bDA/X6YUXrp/recePZTk5TmJYAgDP+/3RQdMrwSAABNa5WaJSKDJ0FtyVhhKXXGtNW9uhzD/jFXQ6Q6GUoqnpwuCvNTY9Df5r2LLlN1lzKYlIBGbOjNDScjG7di0lmfRobJzMrl1L2bhxYSEiQy6D5ub3efHFZdx333zWr/8ArQfasguRSmluuukcjDGICC0tF7N27QHmzq3i+eeXcvjwj/lE4bDi7bf/x9Gjca6+ejbbtn2ObSvSaU13d5pnnvkcK7NuP+PWmOBEZiT1D4ExBrnnnkOu6ybzjpnRwnfdrSLSIsbkexDleSlrvEwyEEgZY+7wYEOexGNau2F4CYiCpIG7HJHmk0FLSXu9FPhqM4ioDENs4CyRdSnL+uGfY7Lw4owo6ibKJlGg5mLqHlFHIqC1wRgzolMbDiMwMkyfHubRRxtwXUM6rRGB/n4XxxlwZiKQSHgkk/7JnttWIiOYNMmmrq6S3l6X3buXceut52JZwqZNi9m+/ULicZedO5eyZMlUbFvYsWMJmzcvzpO+KKNo1MJxDCCD1r+nx8F1DfG4i20LnmeyUomQ49EH66qgMRgDnZ1pYjGLGTPCaO2brtaGN97wt8Zzz31JRYVFLGYTj7tUVlrMmVOFZQnvvdeBUgw6RkY0b88z2LagtUEpoZA9BAajNYRCuVIOSDWieVuWP7hIYSbBCogIlgVaFzbxMrug4vgVMvIPsbK4u2EhK1c+uKGh4Y/bRdSYDz9jtO7p6U+9+uoV0WJ9VGPjrX8ZD5PMfJXWqZBSN71OEXUoY3SZ1k1sradco9TvngKsPEaSwbjZiAAS1rrufsu65E8gg/ZouY9yA1bI85p2KrXwMj/W/0kYkXGrytL68ndEZs3xY4iyMzLk+nBjbvhaJBYl4Dbu4Y1BKYWI8tMBokREjIg2cEevUuHCaZbRQinF1KlTueKKy0kkksFnwRfRQEt5GAWn6bx5C4Y2ZZmVhVGwO5QqqAmBX6X3LtYQxHFaG8pxBRhWIsfRPPlkI2efHWG8R8nwSTsR4nEPx/EDD8fROI6mv9/NbMsBqeNx/1sqlR88wgjBietq5s6tIhazufrq2Vx6aTWzZkUQEV5//Tj79p0ikfB49tmLqKmp4ODBLpYsmcYdd/yHcFgNClKKZ+hEMpGPycZ1s2dHuPvugzzwwEdcf30tVVU2mzYtJhJRrF17gI8+6sre7EdcumA5gr4BQ62FI0fiiEBfn0tfn0tj42Tmz5/ESy8dIxxWvPNOR9FbRUGJXNcwY0bF4I4K6uuriMddtDbEYjZffNHHoUPd3H77HBxHs2pVTXayI0okItTVVbJlSyPxuDtIys7ONHv2LKe1dRnvvdfBDz+k2b79Szo707S2LmPlyprRXcROnUpx2237qa72427X9dMx6bTHmjX7ASEW80kfe+x8LEt47LE2rrxyFiKC6xrCYTOyMRhjiEQsNm8+n+++S3LkSB9dXWna2/uJRm2iUStjIPD445/R3t7Pxo2LOPvsState5+qqvz5Fw3yA+vJZjqzs/OvMkEf8ONzxzHZXHxun2GXLnfgfFOVvD5ak8NkcJ8Ap9+pnmF0htEvn5FtDMZPf5UlOZiHIC2qRNRPwiAXSlkiy5f/eW1Dw3VPhUJVNcZ4fpbAF+4nn8BQGKONbdu6o+MH9dprf3vHmH9da0x/KmguZQzV1PSHVtuurAZj/PpjUHY7PTAGS8SgddVlxtyZUOq6VlCGArfVQlAiygQRGmDKdKEdIwLeBlBhSBmtZ94O9xrLWrUx02nYAFEBkhkok9ebCBDxb90AVgiS2vMWPA4bjFJN12X6FNTYz+Z+xgBh4LYoYNmQ8LRe/g+Re7qVmnNBptsgwSayQAFyMsKiQMQYp0rrqz4UufNzpaZVZ7r9FKmM8iA/oDaQ1ZgxfobJGGOkXuubv1Pq5n+LVCjKlTIpJxzHoaamhvr6eiAOJPEfOmR/mYcPScAJQZ/R2lplzO0O/HZH2epG5UBunC8ihMPhouWhXDKye82UJzNTLuRO3hhDKpUapvcAGTm3lgklUIAxnIMBwcTbQ+PFmAUKirq5HqnQt58b49SQfwG3bcG2JVtvPJ0Y0x4yxs8idHc7rFkzh2uvPQuAt946xe7d/2Xq1FAmVzngsYJtYQzZ9EeA7LGZ6Rf8DxQd0ObSFdtnY3bbAykUqKuLIgLt7f14HkyebDNlSgjH0bS393POOVHmzq0C4OjRPo4fTxCNWoBk0v5Cf7/LlClhFi6MEY3adHQkOXy4l+nTw0yZEiKV0nR0pAbl8wphzF5OKSGR8Fi8eBKbN58PwNatn3HwYDfXXDObG26ozRSHFT09DidPJqirqyQWCwHwyivtvPnmt1iWsHDhJDZtWkwQaR8/nsC2FbNnR7KL9/XXcZ544jPC+W8oyyNQgNyVys05ifir/tBDH9HX5xKNWvT0OCxaNImHHz6fW245l46OFG1tPWzYMB+AvXtPsGfPMaqrKzDG0N3t8OCDC7nkkmq0LpxsHoqSnUJgYp438FxiJG/mOBrX9Z2GXzkX0mmdfS4WlPbz55rrOUudYWbM0roZPM8webKf1J4+PUxr6xHefbcjZ7MPuOuglDNtWpidO5fy5Ze9fPNNgkWLYtTW+gXgffu+Zf/+TkIhxdNPH+bRRxtYvbqW1atrOXEigW0Ls2ZFsjPIrQ8VS6SXLJAx/mp2d6fZu/ckM2dW0Nb2I7YtuG6wvJL1TsEjiRMnEmzZ8ilNTVOor6/iwIEu9uw5xqef9lJRoYhELJTyax0bNnyIUkJNTQXTpoXo6krz1Vd9PPJIAw0Nk/n++xSOo6moKP6GomSBAqFCIcXBg12k05qzzqpk27YmKir8levtdThxIkEoJNmcbTTqT7itrYdPPvkxO05lpZX1bomEx/z5Me6/fwG2XXgHfPxxN62tRzIZ7+FDo1G77UDdvu371YhUSpNIeNlH2J7nm6iIr9kgfhwafAbnlFJCKqVxXU1VlU0kYmWrT6mUJhq1Bp1pw2GMXi6YrCKZ9E3O15RfrLCsAS0F4dBQDBUuFBLCYRtj/HdY4I+fO04pQeuoBRooGYzu+2jHK5V+KM5E2xMdZwSa6Dgj0ETHr04ge6Am5eM0lh7GAZMNPGy/FiQmc7mSQLxfgmA5c8WPjAyWbYePVFcvWGlZFTH/WWWmfPcLEGgAxihlSSLR1f5/pZS9pkmnuUsAAAAASUVORK5CYII=",
			"type":  "png"
		}
	]
	`
	request := httptest.NewRequest(http.MethodPost, "/", strings.NewReader(data))
	request.Header.Set("Content-Type", dao.JsonType)
	recorder := httptest.NewRecorder()
	IndexPost(recorder, request)
	time.Sleep(2 * time.Second)

	response := recorder.Result()
	assert.Equal(t, http.StatusOK, response.StatusCode)

	images, _ := ioutil.ReadDir(config.ImagePath)
	previews, _ := ioutil.ReadDir(config.ImagePreviewPath)

	assert.NotEmpty(t, images)
	assert.NotEmpty(t, previews)

	// form
	os.RemoveAll(config.ImagePath)
	os.RemoveAll(config.ImagePreviewPath)
	os.Mkdir(config.ImagePreviewPath, 0777)
	os.Mkdir(config.ImagePath, 0777)
	createImg("test.png", config)

	file, _ := os.Open(config.ImagePath + "/test.png")
	var body bytes.Buffer
	writer := multipart.NewWriter(&body)
	fw, _ := writer.CreateFormFile("images", "test.png")
	io.Copy(fw, file)
	file.Close()

	request = httptest.NewRequest(http.MethodPost, "/", &body)
	request.Header.Set("Content-Type", writer.FormDataContentType())
	writer.Close()

	recorder = httptest.NewRecorder()
	IndexPost(recorder, request)
	time.Sleep(2 * time.Second)

	response = recorder.Result()
	assert.Equal(t, http.StatusOK, response.StatusCode)

	images, _ = ioutil.ReadDir(config.ImagePath)
	previews, _ = ioutil.ReadDir(config.ImagePreviewPath)

	assert.NotEmpty(t, images)
	assert.NotEmpty(t, previews)

	service.Shutdown()
}

func TestIndexGetInvalid(t *testing.T) {
	// url is empty
	request := httptest.NewRequest(http.MethodGet, "/", nil)
	request = mux.SetURLVars(request, map[string]string{
		"img_url": "",
	})
	recorder := httptest.NewRecorder()
	IndexGet(recorder, request)

	response := recorder.Result()
	assert.Equal(t, http.StatusBadRequest, response.StatusCode)

	// invalid url
	request = httptest.NewRequest(http.MethodGet, "/", nil)
	request = mux.SetURLVars(request, map[string]string{
		"img_url": "not url",
	})
	recorder = httptest.NewRecorder()
	IndexGet(recorder, request)

	response = recorder.Result()
	assert.Equal(t, http.StatusUnprocessableEntity, response.StatusCode)

	// not  image
	request = httptest.NewRequest(http.MethodGet, "/", nil)
	request = mux.SetURLVars(request, map[string]string{
		"img_url": "https://ya.ru",
	})
	recorder = httptest.NewRecorder()
	IndexGet(recorder, request)

	response = recorder.Result()
	assert.Equal(t, http.StatusBadRequest, response.StatusCode)
}

func TestIndexPostInvalid(t *testing.T) {
	// json
	// invalid json
	invalidJson := `{ "test": 1`
	request := httptest.NewRequest(http.MethodGet, "/", strings.NewReader(invalidJson))
	request.Header.Set("Content-Type", dao.JsonType)
	recorder := httptest.NewRecorder()

	IndexPost(recorder, request)

	response := recorder.Result()
	assert.Equal(t, http.StatusUnprocessableEntity, response.StatusCode)

	// invalid img data
	invalidData := `
		[{
			"image": "broken data",
			"type":  "png"
		}]
	`
	request = httptest.NewRequest(http.MethodGet, "/", strings.NewReader(invalidData))
	request.Header.Set("Content-Type", dao.JsonType)
	recorder = httptest.NewRecorder()

	IndexPost(recorder, request)

	response = recorder.Result()
	assert.Equal(t, http.StatusBadRequest, response.StatusCode)

	// form
	// invalid form
	request = httptest.NewRequest(http.MethodPost, "/", nil)
	request.Header.Set("Content-Type", dao.FormType)
	recorder = httptest.NewRecorder()

	IndexPost(recorder, request)

	response = recorder.Result()
	assert.Equal(t, http.StatusBadRequest, response.StatusCode)

	// empty files
	var body bytes.Buffer
	writer := multipart.NewWriter(&body)
	_, _ = writer.CreateFormFile("images", "test.png")

	request = httptest.NewRequest(http.MethodPost, "/", &body)
	request.Header.Set("Content-Type", writer.FormDataContentType())
	recorder = httptest.NewRecorder()
	writer.Close()

	IndexPost(recorder, request)

	response = recorder.Result()
	assert.Equal(t, http.StatusBadRequest, response.StatusCode)

	// not files
	var f bytes.Buffer
	writer = multipart.NewWriter(&f)
	_, _ = writer.CreateFormFile("files", "test.png")

	request = httptest.NewRequest(http.MethodPost, "/", &f)
	request.Header.Set("Content-Type", writer.FormDataContentType())
	recorder = httptest.NewRecorder()
	writer.Close()

	IndexPost(recorder, request)

	response = recorder.Result()
	assert.Equal(t, http.StatusUnprocessableEntity, response.StatusCode)
}

func createImg(name string, config dao.Config) {
	width := 2000
	height := 1000
	upLeft := image.Point{}
	lowRight := image.Point{X: width, Y: height}
	cyan := color.RGBA{R: 100, G: 200, B: 200, A: 0xff}
	img := image.NewRGBA(image.Rectangle{Min: upLeft, Max: lowRight})

	for x := 0; x < width; x++ {
		for y := 0; y < height; y++ {
			switch {
			case x < width/2 && y < height/2: // upper left quadrant
				img.Set(x, y, cyan)
			case x >= width/2 && y >= height/2: // lower right quadrant
				img.Set(x, y, color.White)
			default:
				// Use zero value.
			}
		}
	}

	newImg, _ := os.Create(config.ImagePath + "/" + name)
	png.Encode(newImg, img)
	newImg.Close()
}
