package handler

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strings"

	"image_test/model/dao"
	"image_test/model/service"
)

func IndexGet(w http.ResponseWriter, r *http.Request) {
	imgUrl := r.FormValue("img_url")

	if _, err := url.ParseRequestURI(imgUrl); err != nil {
		writeError(w, http.StatusUnprocessableEntity, "img_url is invalid")
		return
	}
	if err := service.SaveFromUrl(imgUrl); err != nil {
		writeError(w, http.StatusBadRequest, err.Error())
		return
	}
}

func IndexPost(w http.ResponseWriter, r *http.Request) {
	dataType := r.Header.Get("Content-Type")

	if dataType == dao.JsonType {
		var data []dao.ImgData

		if err := json.NewDecoder(r.Body).Decode(&data); err != nil {
			writeError(w, http.StatusUnprocessableEntity, "json data is invalid")
			return
		}
		if errs := service.SaveFromJson(data); len(errs) != 0 {
			writeErrors(w, http.StatusBadRequest, errs)
			return
		}
	}
	if strings.HasPrefix(dataType, dao.FormType) {
		if err := r.ParseMultipartForm(32 << 20); err != nil {
			writeError(w, http.StatusBadRequest, "form data is invalid")
			return
		}
		if len(r.MultipartForm.File["images"]) == 0 {
			writeError(w, http.StatusUnprocessableEntity, "form files is empty")
			return
		}
		if errs := service.SaveFromForm(r.MultipartForm); len(errs) != 0 {
			writeErrors(w, http.StatusBadRequest, errs)
			return
		}
	}
}

func writeError(w http.ResponseWriter, status int, err string) {
	writeErrors(w, status, []error{fmt.Errorf(err)})
}

func writeErrors(w http.ResponseWriter, status int, errs []error) {
	data := dao.ApiError{
		Status: status,
	}
	cnt := len(errs)
	messages := make([]string, cnt)

	for i := 0; i < cnt; i++ {
		messages[i] = errs[i].Error()
	}

	data.Messages = messages
	w.WriteHeader(status)

	if err := json.NewEncoder(w).Encode(data); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}
