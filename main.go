package main

import (
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"

	"image_test/handler"
	"image_test/middleware"
	"image_test/model/dao"
	"image_test/model/service"

	"github.com/gorilla/mux"
)

var conf dao.Config

func init() {
	imgWidth, err := strconv.Atoi(os.Getenv("IMAGE_WIDTH"))

	if err != nil {
		panic("IMAGE_WIDTH is invalid " + err.Error())
	}

	imgHeight, err := strconv.Atoi(os.Getenv("IMAGE_HEIGHT"))

	if err != nil {
		panic("IMAGE_HEIGHT is invalid " + err.Error())
	}

	conf.ImageWidth = imgWidth
	conf.ImageHeight = imgHeight
	conf.Address = strings.TrimSuffix(os.Getenv("SERVICE_ADDRESS"), "/")
	conf.ImagePreviewPath = strings.TrimSuffix(os.Getenv("IMAGE_PREVIEW_PATH"), "/")
	conf.ImagePath = strings.TrimSuffix(os.Getenv("IMAGE_PATH"), "/")

	if err := conf.IsValid(); err != nil {
		panic("configuration is invalid " + err.Error())
	}
}

func run() {
	router := mux.NewRouter()
	router.HandleFunc("/", handler.IndexGet).Methods(http.MethodGet)
	router.HandleFunc("/", handler.IndexPost).Methods(http.MethodPost)
	router.Use(middleware.Middleware)

	log.Println("server start on", conf.Address)

	if err := http.ListenAndServe(conf.Address, router); err != nil {
		panic("start server error: " + err.Error())
	}
}

func main() {
	service.Setup(conf)

	go run()

	osSignalChan := make(chan os.Signal)
	signal.Notify(osSignalChan, syscall.SIGINT, syscall.SIGTERM)
	<-osSignalChan

	service.Shutdown()
}
